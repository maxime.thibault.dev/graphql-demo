from distutils.core import setup

setup(
    name='GraphQL-Poc',
    install_requires=[
        'Flask',
        'Flask_graphql',
        'SQLAlchemy',
        'graphene',
        'graphene-sqlalchemy'
      ]
)