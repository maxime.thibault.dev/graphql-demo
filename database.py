from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine('sqlite:///database.sqlite3', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
    from models import UserModel
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)

    max = UserModel(name='Maxime', last_name='Thibault')
    db_session.add(max)
    math = UserModel(name='Mathieu', last_name='Aubert')
    db_session.add(math)
    val = UserModel(name='Valentin', last_name='Boucher')
    db_session.add(val)
    bert = UserModel(name='Bertin', last_name='Colpron')
    db_session.add(bert)
    db_session.commit()
